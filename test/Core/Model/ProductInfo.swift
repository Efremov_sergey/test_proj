//
//  ProductInfo.swift
//  test
//
//  Created by Sergey on 18.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductInfo: Mappable {
    var productId: Int?
    var title: String?
    var productDescription: String?
    var price: Int?
    var rating: Int?
    var imageUrl: String?
    var images: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        productId           <- map["productId"]
        title               <- map["title"]
        productDescription  <- map["productDescription"]
        price               <- map["price"]
        rating              <- map["rating"]
        imageUrl            <- map["imageUrl"]
        images              <- map["images"]
    }
}
