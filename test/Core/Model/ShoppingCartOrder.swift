//
//  ShoppingCartOrder.swift
//  test
//
//  Created by Sergey on 28.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper

class ShoppingCartOrder: Mappable {
    var productId: Int?
    var itemCount: Int?
    
    init(productId: Int, itemCount: Int) {
        self.productId = productId
        self.itemCount = itemCount
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.productId          <- map["productId"]
        self.itemCount      <- map["itemCount"]
    }
}
