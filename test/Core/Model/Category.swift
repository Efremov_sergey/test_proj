import ObjectMapper
import RealmSwift

class Category: Object, Mappable {
    @objc dynamic var  categoryId: Int = Int(0)
    @objc dynamic var  title: String = String()
    @objc dynamic var  imageUrl: String = String()
    @objc dynamic var  hasSubcategories: Int = Int(0)
    @objc dynamic var  fullName: String = String()
    @objc dynamic var  categoryDescription: String = String()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "categoryId"
    }

    func mapping(map: Map) {
        categoryId          <- map["categoryId"]
        title               <- map["title"]
        imageUrl            <- map["imageUrl"]
        hasSubcategories    <- map["hasSubcategories"]
        fullName            <- map["fullName"]
        categoryDescription <- map["categoryDescription"]
    }
    
    func save() {
        do {
            let realm = try! Realm()
            
            try realm.write {
                print(Realm.Configuration.defaultConfiguration.fileURL as Any)
                if realm.object(ofType: Category.self, forPrimaryKey: self.categoryId) == nil {
                    if categoryId != 0 {
                        realm.add(self)
                    }
                }
            }
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
}
