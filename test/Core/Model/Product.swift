//
//  Product.swift
//  test
//
//  Created by Sergey on 13.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Product: Object, Mappable {
    @objc dynamic var categoryId: Int = Int(0)
    
    @objc dynamic var productId: Int = Int(0)
    @objc dynamic var title: String?
    @objc dynamic var productDescription: String?
    @objc dynamic var price: Int = Int(0)
    @objc dynamic var rating: Int = Int(0)
    @objc dynamic var imageUrl: String?
    
    var images: [String] {
        get {
            return _backingImages.map { $0.stringValue }
        }
        set {
            _backingImages.removeAll()
            _backingImages.append(objectsIn: newValue.map { RealmString(value: [$0]) })
        }
    }
    var _backingImages = List<RealmString>()
    
    override static func ignoredProperties() -> [String] {
        return ["images"]
    }
    
    override class func primaryKey() -> String? {
        return "productId"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func isEquals(product: Product) -> Bool {
        return (product.productId == self.productId)
    }
    
    func mapping(map: Map) {
        productId           <- map["productId"]
        title               <- map["title"]
        productDescription  <- map["productDescription"]
        price               <- map["price"]
        rating              <- map["rating"]
        imageUrl            <- map["imageUrl"]
        images              <- map["images"]
    }
    
    func save(categoryId: Int) {
        do {
            let realm = try! Realm()
            
            try realm.write {
                print(Realm.Configuration.defaultConfiguration.fileURL as Any)
                if realm.object(ofType: Product.self, forPrimaryKey: self.productId) == nil {
                    if categoryId != 0 {
                        self.categoryId = categoryId
                        realm.add(self)
                    }
                }
            }
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
}

class RealmString : Object {
    @objc dynamic var stringValue = ""
}
