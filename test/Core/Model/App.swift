//
//  APP.swift
//  test
//
//  Created by Sergey on 04.05.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation

class App {
    static let sharedInstance: App = {
        let instance = App()
        return instance
    }()
    
    //MARK: Local Variable
    
    var shoppingCart = ShoppingCart()
    var categoryAPI = CategoryAPI()
    var orderAPI = OrderAPI()
    var promoAPI = PromoAPI()
    var productAPI = ProductAPI()
    
    //MARK: Init
    
    private init() {
    }
}
