//
//  MenuItem.swift
//  test
//
//  Created by Sergey on 25.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import UIKit

class MenuItem {
    var title: String
    var icon: UIImage?
    var storybord: String?
    var type: String?
    var controllerId: String?
    
    init(title: String, icon: UIImage, controllerId: String) {
        self.title = title
        self.icon = icon
        self.controllerId = controllerId
    }
}
