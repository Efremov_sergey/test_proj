//
//  Extensions.swift
//  test
//
//  Created by Sergey on 04.05.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation

extension Int {
    func formatAsPrice() -> String {
        let price = self as NSNumber
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "ru_RU")
        return formatter.string(from: price)!
    }
}
