//
//  ProductListDataProvider.swift
//  test
//
//  Created by Sergey on 27.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import RealmSwift

class ProductListDataProvider: BaseDataProvider {
    var categoryId: Int
    
    init(categoryId: Int) {
        self.categoryId = categoryId
    }
    
    override func loadNextItems() {
        super.loadNextItems()
        if !self.isAllLoaded {
            if Connectivity.Connectivity.isConnectedToInternet {
                if self.isLoading {
                    App.sharedInstance.productAPI.loadProduct(categoryId: self.categoryId, offset: self.items.count, completionHandler: { (items) in
                        for item in items {
                            item.save(categoryId: self.categoryId)
                        }
                        self.onNewItemsLoaded(nextItems: items)
                    })
                }
            } else {
                //No internet
                let items = try! Realm().objects(Product.self).sorted(byKeyPath: "title", ascending: true)
                var result: [Product] = []
                if self.categoryId == 0 {
                    self.isAllLoaded = true
                    self.onNewItemsLoaded(nextItems: items.map{$0})
                    return
                }
                
                for item in items {
                    if item.categoryId == self.categoryId {
                        result.append(item)
                    }
                }
                self.onNewItemsLoaded(nextItems: result)
                self.isAllLoaded = true
            }
        }
    }
}
