//
//  SharesDataProvider.swift
//  test
//
//  Created by Sergey on 27.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation

class PromoDataProvider: BaseDataProvider {
    override func loadNextItems() {
        super.loadNextItems()
        App.sharedInstance.promoAPI.loadPromo(offset: self.items.count) { (items) in
            self.onNewItemsLoaded(nextItems: items)
        }
    }
}
