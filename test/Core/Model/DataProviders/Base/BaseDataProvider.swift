//
//  BaseDataProvider.swift
//  test
//
//  Created by Sergey on 27.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation

protocol DataProviderDelegate: class {
    func dataProvider(_ dataProvider: BaseDataProvider)
}

class BaseDataProvider {
    var isAllLoaded = false
    var isLoading = false
    
    var delegate: DataProviderDelegate?
    
    var items: [Any] = []
    var onComplete: ((_ items: [Any])->())?
    
    func reloadData() {
        self.items = []
        loadNextItems()
    }
    
    func loadNextItems() {
        if !self.isAllLoaded && !self.isLoading {
            loadNextItemsImpl()
        }
    }
    
    func loadNextItemsImpl() {
        self.isLoading = true
    }
    
    func checkState() -> Bool {
        return self.isAllLoaded && self.isLoading
    }
    
    func onNewItemsLoaded(nextItems: [Any]) {
        self.delegate?.dataProvider(self)
        self.isLoading = false
        if nextItems.count == 0 {
            self.isAllLoaded = true
        }
        self.items += nextItems
        //TODO: call async
        self.onComplete?(self.items)
    }
}
