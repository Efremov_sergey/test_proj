//
//  CategoriesDataProvider.swift
//  test
//
//  Created by Sergey on 03.05.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import RealmSwift

class CategoriesDataProvider: BaseDataProvider {
    
    override func loadNextItems() {
        super.loadNextItems()
        if !checkState() {
            if Connectivity.Connectivity.isConnectedToInternet {
                App.sharedInstance.categoryAPI.loadCategories(completionHandler: { (items) in
                    for item in items {
                        item.save()
                    }
                    self.onNewItemsLoaded(nextItems: items)
                })
            } else {
                //No internet
                let items = try! Realm().objects(Category.self)
                self.onNewItemsLoaded(nextItems: items.map{$0})
                self.isAllLoaded = true
            }
        }
    }
    
    func saveItems(nextItems: [Category]) {
        do {
            
            let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
            Realm.Configuration.defaultConfiguration = config
            
            let realm = try! Realm()
            
            try realm.write {
                print(Realm.Configuration.defaultConfiguration.fileURL as Any)
                for item in nextItems {
                    if realm.object(ofType: Category.self, forPrimaryKey: item.categoryId) == nil {
                        realm.add(item)
                    }
                }
            }
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
}
