//
//  Shares.swift
//  test
//
//  Created by Sergey on 25.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper

class Promo: Mappable {
    var postId: Int?
    var categoryId: Int?
    //var creator: String?
    var name: String?
    var image: String?
    var title: String?
    var text: String?
    var numberLikes: Int?
    var numberComments: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.postId          <- map["postId"]
        self.categoryId      <- map["categoryId"]
        self.name            <- map["name"]
        self.image           <- map["image"]
        self.title           <- map["title"]
        self.text            <- map["text"]
        self.numberLikes     <- map["numberLikes"]
        self.numberComments  <- map["numberComments"]
    }
}
