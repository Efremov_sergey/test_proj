//
//  CategoryAPI.swift
//  test
//
//  Created by Sergey on 16.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ProductAPI: BaseAPI {
    func loadProduct(categoryId: Int, offset: Int, completionHandler: @escaping ([Product]) -> Void) {
        load(params: makeParams(categoryId: categoryId, offset: offset), method: .get, url: "product/list", completionHandler: { (data) in
            let json = data as! [[String: Any]]
            let products = Mapper<Product>().mapArray(JSONArray: json)
            //print(products.count)
            completionHandler(products)
        })
    }
    
    func loadInfoProduct(productId: Int, completionHandler: @escaping (Product) -> Void) {
        load(params: ["productId": String(productId), ], method: .get, url: "product/details", completionHandler: { (data) in
            let json = data as! [String: Any]
            let product = Mapper<Product>().map(JSON: json)
            completionHandler(product!)
        })
    }
    
    func makeParams(categoryId: Int, offset: Int) -> [String: String] {
        if categoryId == 0 {
            return ["offset": String(offset)]
        } else {
            return ["categoryId": String(categoryId), "offset": String(offset)]
        }
    }
}
