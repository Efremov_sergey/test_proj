//
//  BaseAPI.swift
//  test
//
//  Created by Sergey on 16.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import Alamofire

class BaseAPI {
    var SERVER_URL = "http://82.146.53.185:8101/api/common/"
    
    func load(params: [String: String], method: HTTPMethod, url: String, completionHandler: @escaping (Any) -> Void) {
        let preparedParams = prepareParams(params: params)
        Alamofire.request(SERVER_URL + url, method: method, parameters: preparedParams).validate().responseJSON { response in
            switch response.result {
            case .success:
                let jsonResponse = response.result.value as! [String: Any]
                //print(jsonResponse)
                //print("--------")
                let data = jsonResponse["data"] as Any
                completionHandler(data);
            case .failure(let error):
                print(error.localizedDescription)
                completionHandler(error.localizedDescription);
            }
        }
    }
    
    func getCommonParams() -> [String: String] {
        return ["appKey": "yx-1PU73oUj6gfk0hNyrNUwhWnmBRld7-SfKAU7Kg6Fpp43anR261KDiQ-MY4P2SRwH_cd4Py1OCY5jpPnY_Viyzja-s18njTLc0E7XcZFwwvi32zX-B91Sdwq1KeZ7m",
            "accessToken": "123"]
    }
    
    func prepareParams(params: [String: String]) -> [String: String] {
        var preparedParams = params
        for param in getCommonParams() {
            preparedParams[param.key] = param.value
        }
        return preparedParams
    }
    
    func prepareUrl(url: String) -> String {
        return SERVER_URL + url
    }
}
