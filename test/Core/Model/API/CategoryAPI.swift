//
//  CategoryAPI.swift
//  test
//
//  Created by Sergey on 16.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper

class CategoryAPI: BaseAPI {
    func loadCategories(completionHandler: @escaping (Array<Category>) -> Void) {
        load(params: ["": ""], method: .get, url: "category/list", completionHandler: { (data) in
            var json = data as! [String: Any]
            let categoriesJson = json["categories"] as! [[String: Any]]
            let categories = Mapper<Category>().mapArray(JSONArray: categoriesJson)
            completionHandler(categories)
        })
    }
}
