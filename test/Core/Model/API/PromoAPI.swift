//
//  SharesAPI.swift
//  test
//
//  Created by Sergey on 25.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper

class PromoAPI: BaseAPI {
    func loadPromo(offset: Int, completionHandler: @escaping ([Promo]) -> Void) {
        load(params:  ["offset": String(offset)], method: .get, url: "promo/list", completionHandler: { (data) in
            let json = data as! [String: Any]
            let sharesJson = json["posts"] as! [[String: Any]]
            print(sharesJson)
            let sharesList = Mapper<Promo>().mapArray(JSONArray: sharesJson)
            print(sharesList.count)
            completionHandler(sharesList)
        })
    }
}
