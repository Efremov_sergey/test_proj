//
//  OrderAPI.swift
//  test
//
//  Created by Sergey on 26.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation

class OrderAPI: BaseAPI {
    var shoppingCartOrder: [ShoppingCartOrder] = []
    
    func loadOrder(phone: String, address: String, paymentType: Int, completionHandler: @escaping (String) -> Void) {
        for item in ShoppingCart.sharedInstance.shoppingCartItems {
            self.shoppingCartOrder.append(ShoppingCartOrder(productId: Int((item.product?.productId)!), itemCount: item.count!))
        }
        
        
        load(params:  ["phone": phone, "address": address, "paymentType": String(paymentType), "shoppingCartItems": shoppingCartOrder.toJSONString()!], method: .post, url: "order/create", completionHandler: { (data) in
            let json = data as! [String: Any]
            print(json)
        })
    }
    
    
}
