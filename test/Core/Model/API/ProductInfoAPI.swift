//
//  ProductInfoAPI.swift
//  test
//
//  Created by Sergey on 18.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ProductInfoAPI: BaseAPI {
    //TODO: move to ProductAPI
    func loadProduct(productId: Int, completionHandler: @escaping (Product) -> Void) {
        load(params: ["productId": String(productId), ], method: .get, url: "product/details", completionHandler: { (data) in
            let json = data as! [String: Any]
            let product = Mapper<Product>().map(JSON: json)
            completionHandler(product!)
        })
    }
}
