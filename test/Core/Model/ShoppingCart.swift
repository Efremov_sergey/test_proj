//
//  ShorringCart.swift
//  test
//
//  Created by Sergey on 23.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper

class ShoppingCart {
    //MARK: Shared Instance
    
    static let sharedInstance: ShoppingCart = {
        let instance = ShoppingCart()
        return instance
    }()
    
    //MARK: Local Variable

    var shoppingCartItems: [ShoppingCartItem] = []

    //MARK: Init

    public init() {
        load()
    }

    func addProduct(_ product: Product) {
        var currentShoppingCartItem: ShoppingCartItem? = nil

        if self.shoppingCartItems.count == 0 {
            currentShoppingCartItem = ShoppingCartItem(product: product, count: 1)
            self.shoppingCartItems.append(currentShoppingCartItem!)
            //self.shoppingCartItems?.append(currentShoppingCartItem!)
            save()
            return
        }

        for shoppingCartItem in self.shoppingCartItems {
            if (shoppingCartItem.product?.isEquals(product: product))! {
                currentShoppingCartItem = shoppingCartItem
            }
        }
        if currentShoppingCartItem == nil {
            currentShoppingCartItem = ShoppingCartItem(product: product, count: 0)
            self.shoppingCartItems.append(currentShoppingCartItem!)
        }
        currentShoppingCartItem?.count = (currentShoppingCartItem?.count)! + 1
        save()
    }


    //NSUserDefaults

    private func save() {
        let json = self.shoppingCartItems.toJSONString()
        UserDefaults.standard.set(json, forKey: "products")
    }

    private func load() {
        if UserDefaults.standard.string(forKey: "products") != nil {
            let json = UserDefaults.standard.string(forKey: "products")
            self.shoppingCartItems = Mapper<ShoppingCartItem>().mapArray(JSONString: json!)!
        }
    }

    public func clear() {
        UserDefaults.standard.removeObject(forKey: "products")
        self.shoppingCartItems = []
    }
}
