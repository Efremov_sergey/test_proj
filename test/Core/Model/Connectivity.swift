//
//  Connectivity.swift
//  test
//
//  Created by Sergey on 04.05.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    struct Connectivity {
        static let sharedInstance = NetworkReachabilityManager()!
        static var isConnectedToInternet:Bool {
            return self.sharedInstance.isReachable
        }
    }
}
