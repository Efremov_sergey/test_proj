//
//  ShoppingCartItem.swift
//  test
//
//  Created by Sergey on 23.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import Foundation
import ObjectMapper

class ShoppingCartItem: Mappable {
    
    var product: Product?
    var count: Int?
    
    init(product: Product, count: Int) {
        self.product = product
        self.count = count
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.product          <- map["product"]
        self.count            <- map["count"]
    }
}
