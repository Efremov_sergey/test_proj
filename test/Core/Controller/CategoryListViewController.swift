//
//  ViewController.swift
//  test
//
//  Created by Sergey on 12.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryListViewController: BaseViewController,  UICollectionViewDelegate,  UICollectionViewDataSource, DataProviderDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories: Array<Category> = []
    var selectedCategory: Category?
    var categotyListDataProvider: CategoriesDataProvider?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Категории"
        loadData()
    }
    
    func dataProvider(_ dataProvider: BaseDataProvider) {
        categotyListDataProvider?.onComplete = { items in
            self.hideHud()
            self.categories = items as! [Category]
            self.collectionView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareCollectionViewSizeCell(collectionView: collectionView, width: self.collectionView.frame.width, height: self.collectionView.frame.height / 4)
    }
    
    func loadData() {
        categotyListDataProvider = CategoriesDataProvider()
        categotyListDataProvider?.delegate = self
        categotyListDataProvider?.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let controller = segue.destination as? ProductListViewController {
            controller.category = self.selectedCategory
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.selectedCategory = nil
        } else {
            self.selectedCategory = self.categories[indexPath.row]
        }
        self.performSegue(withIdentifier: "ProductList", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CategoryCollectionViewCell
        if indexPath.section == 0 {
            cell.labelTitle.text = "Все товары"
            cell.labelSubcategories.isHidden = true
            cell.imageView.image = #imageLiteral(resourceName: "no_image")
        } else {
            let category = self.categories[indexPath.row]
            cell.labelTitle.text = category.title
            cell.labelSubcategories.text = category.categoryDescription
            if (!category.imageUrl.isEmpty) {
                cell.imageView.sd_setImage(with: URL(string: category.imageUrl))
            } else {
                cell.imageView.image = #imageLiteral(resourceName: "no_image")
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? CategoryCollectionReusableView{
            switch indexPath.section {
            case 0:
                sectionHeader.sectionHeaderlabel.text = "Все товары"
                break
            case 1:
                sectionHeader.sectionHeaderlabel.text = "Категории"
                break
            default:
                break
            }
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (section == 0) ? 1 : self.categories.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
