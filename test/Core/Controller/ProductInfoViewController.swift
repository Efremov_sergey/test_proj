//
//  ProductInfoViewController.swift
//  test
//
//  Created by Sergey on 18.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ProductInfoViewController: BaseViewController,  UICollectionViewDelegate, UICollectionViewDataSource {
    
    var product: Product?
    var productInfo: Product?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = product?.title
        loadData()
    }
    
    @IBAction func buttonAddToCart(_ sender: UIButton) {
        ShoppingCart.sharedInstance.addProduct(self.product!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareCollectionViewSizeCell(collectionView: collectionView, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
        prepareCollectionViewSizeCell(collectionView: imageCollectionView, width: self.view.frame.width, height: self.view.frame.height)
    }
    
    @IBAction func didImageSwipe(_ sender: UIPanGestureRecognizer) {
        self.imageCollectionView.isHidden = true
    }
    
    func loadData() {
        App.sharedInstance.productAPI.loadInfoProduct(productId: (product?.productId)!) { (product) in
            self.productInfo = product
            self.displayProduct()
        }
        hideHud()
    }
    
    func displayProduct() {
        if self.productInfo?.productDescription != nil {
            self.labelDescription.text = self.productInfo?.productDescription
        } else {
            self.labelDescription.text = "Нет описания"
        }
        
        if self.productInfo?.price != nil {
            self.labelPrice.text = self.productInfo?.price.formatAsPrice()
        }
        else {
            self.labelPrice.text = "Цена не указана"
        }
        self.collectionView.reloadData()
        self.imageCollectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductInfoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ProductInfoCollectionViewCell

        if self.productInfo?.images != nil {
            let product = self.productInfo?.images[indexPath.row]
            if (product != nil) {
                cell.imageView.sd_setImage(with: URL(string: product!))
            }
        } else {
            cell.imageView.image = #imageLiteral(resourceName: "no_image")
        }
        
        let swipeUpGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(didImageSwipe))
        swipeUpGestureRecognizer.direction = .up
        cell.imageView.addGestureRecognizer(swipeUpGestureRecognizer)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.productInfo?.images == nil {
            return 1
        }
        return (self.productInfo?.images.count)!
    }
    
    private func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> Int {
        if self.productInfo?.images == nil {
            return 1
        }
        return (self.productInfo?.images.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if self.productInfo?.images != nil {
                self.imageCollectionView.isHidden = false
            }
    }
}


