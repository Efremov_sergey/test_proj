//
//  ShopingCartViewController.swift
//  test
//
//  Created by Sergey on 23.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class ShopingCartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var segmentPayment: UISegmentedControl!
    @IBOutlet weak var textPhone: UITextField!
    @IBOutlet weak var textCity: UITextField!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ShoppingCart.sharedInstance.shoppingCartItems.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func showPopup(_ sender: UIButton) {
        viewBackground.isHidden = false
        viewPopUp.isHidden = false
    }
    
    private func tableView(_ tableView: UITableView, cellForItemAt indexPath: IndexPath) -> Int {
        return ShoppingCart.sharedInstance.shoppingCartItems.count
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ShoppingCartTableViewCell
        let shoppingCartItem = ShoppingCart.sharedInstance.shoppingCartItems[indexPath.row]
        cell.labelTitle.text = shoppingCartItem.product?.title
        if shoppingCartItem.product?.price != nil {
            cell.labelPrice.text = shoppingCartItem.product?.price.formatAsPrice()
        } else {
            cell.labelPrice.text = "Цена не указана"
        }
        cell.labelCount.text = String(shoppingCartItem.count!)
        if (shoppingCartItem.product?.imageUrl != nil) {
            cell.imageViewSC?.sd_setImage(with: URL(string: (shoppingCartItem.product?.imageUrl!)!))
        } else {
            cell.imageViewSC?.image = #imageLiteral(resourceName: "no_image")
        }
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Очистить", style: .plain, target: self, action: #selector(rightBarButtonItemClick))
        
        var total = 0;
        if (ShoppingCart.sharedInstance.shoppingCartItems.count != 0) {
            for shoppingCartItem in ShoppingCart.sharedInstance.shoppingCartItems {
                if shoppingCartItem.product?.price != nil {
                    total += (shoppingCartItem.product?.price)! * shoppingCartItem.count!
                }
            }
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cancelView))
        tapGestureRecognizer.numberOfTapsRequired = 1
        viewBackground.addGestureRecognizer(tapGestureRecognizer)
        labelCost.text = total.formatAsPrice()
        
        self.tableView.rowHeight = self.view.frame.height / 8
    }
    
    @IBAction func cancelView(_ sender: UIButton) {
        viewBackground.isHidden = true
        viewPopUp.isHidden = true
    }
    
    @IBAction func payButtonClick(_ sender: UIButton) {
        let order = OrderAPI()
        order.loadOrder(phone: textPhone.text!, address: textCity.text!, paymentType: segmentPayment.selectedSegmentIndex + 1) { (data) in
            print(data)
        }
    }
    
    
    @IBAction func rightBarButtonItemClick(_ sender: AnyObject) {
        ShoppingCart.sharedInstance.clear()
        tableView.reloadData()
    }
}
