//
//  MenuItemViewController.swift
//  test
//
//  Created by Sergey on 25.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var menuItems: [MenuItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuItems.append(MenuItem(title: "Категории", icon: #imageLiteral(resourceName: "promo"), controllerId: "Category"))
        menuItems.append(MenuItem(title: "Корзина", icon: #imageLiteral(resourceName: "shoppingcart"), controllerId: "ShoppingCart"))
        menuItems.append(MenuItem(title: "Акции", icon: #imageLiteral(resourceName: "category"), controllerId: "Promo"))
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menuItems.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewCell", for: indexPath) as! MenuItemViewCell
        
        cell.labelName.text = menuItems[indexPath.row].title
        cell.imageViewIcon.image = menuItems[indexPath.row].icon
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controllerView = storyboard!.instantiateViewController(withIdentifier: menuItems[indexPath.row].controllerId!)
        self.navigationController?.show(controllerView, sender: nil)
    }
}
