//
//  ProductViewController.swift
//  test
//
//  Created by Sergey on 16.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "Cell"

class ProductListViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, DataProviderDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelNoData: UILabel!
    
    var category: Category?
    var products: [Product] = []
    var selectedProduct: Product?
    var productListDataProvider: ProductListDataProvider?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if category != nil {
            self.title = category?.fullName
        } else {
            self.title = "Все товары"
        }
        productListDataProvider?.delegate = self
        self.labelNoData.isHidden = true
        showHud()
        productListDataProvider?.reloadData()
        prepareParamsForRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareCollectionViewSizeCell(collectionView: collectionView, width: self.collectionView.frame.width, height: self.collectionView.frame.height / 3)
    }
    
    func prepareParamsForRequest() {
        if category != nil {
            productListDataProvider = ProductListDataProvider(categoryId: (self.category?.categoryId)!)
        } else {
            productListDataProvider = ProductListDataProvider(categoryId: 0)
        }
        productListDataProvider?.delegate = self
        productListDataProvider?.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let controller = segue.destination as? ProductInfoViewController {
            controller.product = self.selectedProduct
        }
    }
    
    func dataProvider(_ dataProvider: BaseDataProvider) {
        productListDataProvider?.onComplete = { items in
            self.hideHud()
            self.products = items as! [Product]
            self.collectionView.reloadData()
        }
    }
    
    //UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedProduct = self.products[indexPath.row]
        self.performSegue(withIdentifier: "Product", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.products.count - 1 {
            productListDataProvider?.loadNextItems()
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.products.count
    }
    
    private func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.products.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProductCollectionViewCell
        
        let product = self.products[indexPath.row]
        cell.labelTitle.text = product.title
        if (product.imageUrl != nil) {
            cell.imageView.sd_setImage(with: URL(string: product.imageUrl!))
        } else {
            cell.imageView.image = #imageLiteral(resourceName: "no_image")
        }
    
        return cell
    }

}
