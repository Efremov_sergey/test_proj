//
//  SharesViewController.swift
//  test
//
//  Created by Sergey on 25.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class PromoViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, DataProviderDelegate {
    
    var isAllLoaded = false
    var isLoading = false
    var promoList: [Promo] = []
    var promoDataProvider: PromoDataProvider? = PromoDataProvider()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bvc = BaseViewController()
        bvc.prepareCollectionViewSizeCell(collectionView: collectionView, width: self.view.frame.width, height: self.view.frame.height / 3.5 )
        
        self.title = "Акции"
        
        promoDataProvider?.delegate = self
        showHud()
        promoDataProvider?.reloadData()
    }


    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.promoList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PromoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PromoCollectionViewCell
        
        let sharesItem = self.promoList[indexPath.row]
        cell.labelTitle.text = sharesItem.title
        cell.labelComments.text = String(sharesItem.numberComments!)
        cell.labelLike.text = String(sharesItem.numberLikes!)
        cell.labelText.text = sharesItem.text
        cell.labelText.numberOfLines = 2
        return cell
    }
    
    // MARK: ItemsToCollectionView
    func dataProvider(_ dataProvider: BaseDataProvider) {
        promoDataProvider?.onComplete = { items in
            self.hideHud()
            self.promoList = items as! [Promo]
            self.collectionView.reloadData()
        }
    }
}
