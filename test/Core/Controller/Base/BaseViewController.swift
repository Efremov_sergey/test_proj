//
//  BaseViewController.swift
//  test
//
//  Created by Sergey on 23.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit
import MBProgressHUD
import SideMenu

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tryShowMenuButton()
    }
    
    func tryShowMenuButton() {
        if self.navigationController?.viewControllers.count == 1 {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Меню", style: .plain, target: self, action: #selector(leftBarButtonItemClick))
        }
    }
    
    func showHud() {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Загрузка"
    }
    
    func hideHud() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @IBAction func leftBarButtonItemClick(_ sender: AnyObject) {
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuPushStyle = .popWhenPossible
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        self.navigationController?.show(SideMenuManager.default.menuLeftNavigationController!, sender: nil)
    }
    
    public func prepareCollectionViewSizeCell(collectionView: UICollectionView, width: CGFloat, height: CGFloat) {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width - layout.sectionInset.left - layout.sectionInset.right, height: height - layout.sectionInset.top - layout.sectionInset.bottom)
        layout.invalidateLayout()
    }
}

