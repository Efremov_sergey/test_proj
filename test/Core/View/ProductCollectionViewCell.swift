//
//  ProductCollectionViewCell.swift
//  test
//
//  Created by Sergey on 16.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}
