//
//  CategoryCollectionViewCell.swift
//  test
//
//  Created by Sergey on 13.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle: UILabel!

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var labelSubcategories: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
}
