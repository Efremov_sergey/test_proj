//
//  ShoppingCartTableViewCell.swift
//  test
//
//  Created by Sergey on 23.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class ShoppingCartTableViewCell: UITableViewCell {
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var imageViewSC: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
