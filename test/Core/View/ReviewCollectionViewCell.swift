//
//  ReviewCollectionViewCell.swift
//  test
//
//  Created by Sergey on 28.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit
import AARatingBar

class ReviewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var rating: AARatingBar!
}
