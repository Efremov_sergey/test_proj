//
//  SharesCollectionViewCell.swift
//  test
//
//  Created by Sergey on 25.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class PromoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var labelLike: UILabel!
    @IBOutlet weak var labelComments: UILabel!
}
