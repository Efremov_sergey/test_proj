//
//  MenuItemViewCell.swift
//  test
//
//  Created by Sergey on 25.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class MenuItemViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
