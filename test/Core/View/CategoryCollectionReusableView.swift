//
//  CategoryCollectionReusableView.swift
//  test
//
//  Created by Sergey on 20.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class CategoryCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var sectionHeaderlabel: UILabel!
}
