//
//  ProductInfoCollectionViewCell.swift
//  test
//
//  Created by Sergey on 17.04.2018.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class ProductInfoCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!

    func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / self.imageView.bounds.width
        let heightScale = size.height / self.imageView.bounds.height
        let minScale = min(widthScale, heightScale)
        
        self.scrollView.minimumZoomScale = minScale
        self.scrollView.zoomScale = minScale
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        let gestureRecognizers = self.imageView.gestureRecognizers
        for gestureRecognizer in gestureRecognizers! {
            self.imageView.removeGestureRecognizer(gestureRecognizer)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    override func awakeFromNib() {
        if scrollView != nil {
            self.scrollView.minimumZoomScale = 0.5
            self.scrollView.maximumZoomScale = 3.5
            self.scrollView.delegate = self
            
            let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
            doubleTapGestureRecognizer.numberOfTapsRequired = 2
            self.scrollView.addGestureRecognizer(doubleTapGestureRecognizer)
        }
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let imageViewSize = imageView.frame.size
        let scrollViewSize = scrollView.bounds.size
        
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
    
    @IBAction func handleDoubleTap(_ sender: UITapGestureRecognizer) {
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }
}
